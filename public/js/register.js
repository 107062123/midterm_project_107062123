var db = firebase.database();

async function clickRegister() {
    event.preventDefault();
    var email = GEBI('email');
    var pwd = GEBI('pwd');

    try {
        await firebase.auth().createUserWithEmailAndPassword(email.value, pwd.value);
        firebase.auth().signInWithEmailAndPassword(email.value, pwd.value);
        var user = firebase.auth().currentUser;
        if (user) {
            db.ref(processEmailToHyphen(email.value)).set({
                email:email.value
            });
            window.location.href = 'chat.html';
        }
    } catch (error) {
        alert(error.message);
    }

}

GEBI('register').addEventListener('click',clickRegister);