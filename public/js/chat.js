var user = '';
var user_email_hyphen = '';
var database = firebase.database();
var ref1 = '';
var ref2 = '';
var room_name = '';
var chat_room_list = [];
var messaging = firebase.messaging();
messaging.usePublicVapidKey('BGOK5GNre25I4hWBmp_BDlKgENm4Q5j-_FLPljk_eYTDTgzp72vRKoUMBn2tvmyZRmC4VnqUTjtTXbhWbA_Ngfw');

function chatting(data){
    var chat = document.getElementById('chat');
    var frame = document.createElement('div');
    var msg = document.createElement('p');
    if(data.val().msg == 'babyrage'){
        for(var k = 0; k < 5; k++)
        msg.innerHTML += '<img src="img/babyrage.png" alt="babyrage" id="sticker" class="animated heartBeat infinite">';
    }else{
        msg.innerText += data.val().msg;
    }
    if (data.val().sender == user.email) {
        frame.className = 'frame-right animated zoomIn';
        frame.innerHTML = "<span>" + user.email + ":</span>";
    } else {
        frame.className = 'frame-left animated zoomIn';
        frame.innerHTML = "<span>" + room_name + ":</span>";
    }
    msg.className = 'message';
    frame.appendChild(msg);
    chat.appendChild(frame);
    chat.scrollTop = chat.scrollHeight;
}
//click room name to chat
function changeRoom() {
    if (ref1 != '') ref1.off('child_added',chatting);
    room_name = this.innerHTML;
    GEBI('chat-room-select').innerText = room_name;
    ref1 = database.ref(user_email_hyphen + '/' + processEmailToHyphen(room_name));
    ref2 = database.ref(processEmailToHyphen(room_name) + '/' + user_email_hyphen);

    var chat = document.getElementById('chat');
    chat.innerHTML = '';
    ref1.on('child_added',chatting);
}

// invite an user to chat
async function sendUserEmail() {
    if (event.keyCode == '13') {//press enter
        event.preventDefault();
        var partnerEmail = this.value;
        if (ref1 != '') ref1.off('child_added',chatting);
        if (partnerEmail == user.email) {
            alert('Cannot create a chat room with yourself!');
            return;
        }
        var ref = database.ref();
        var email_exist = false;//check whether the person is one of member of this app
        await ref.once('value', function (snapshot) {
            for (i in snapshot.val()) {
                if (snapshot.val()[i].email == partnerEmail) {
                    email_exist = true;
                    break;
                }
            }
        });
        if (email_exist == false) {
            alert("the person doesn't exist!");
            return;
        }

        email_exist = false;//check whether the person's room has already established
        for (i in chat_room_list) {
            if (chat_room_list[i] == partnerEmail) {
                email_exist = true;
                break;
            }
        }
        if (email_exist == false) {
            database.ref(user_email_hyphen + '/chat_room').push({
                partner: partnerEmail
            });
            database.ref(processEmailToHyphen(partnerEmail) + '/chat_room').push({
                partner: user.email
            });
        }

        ref1 = database.ref(user_email_hyphen + '/' + processEmailToHyphen(partnerEmail));
        ref2 = database.ref(processEmailToHyphen(partnerEmail) + '/' + user_email_hyphen);
        var chat = document.getElementById('chat');
        chat.innerText = '';
        ref1.on('child_added', chatting);
        GEBI('chat-room-select').innerText = partnerEmail;
        this.value = '';
    }
}

function clickSubmit() {
    if (ref1 == '') {
        alert('please invite a person first!');
        return;
    }
    var msg = document.getElementById('msg');
    var date = new Date();
    if (msg.value != '') {
        ref1.push({
            sender: user.email,
            msg: msg.value,
            time: date.getTime()
        });
        ref2.push({
            sender: user.email,
            msg: msg.value,
            time: date.getTime()
        })
        msg.value = '';
    }
}

function Noti(snapshot){
    messaging.getToken().then(function(token){
        if(processEmailToHyphen(snapshot.val().sender) == user_email_hyphen)return;
        var notification = {
            'title': snapshot.val().sender + " :",
            'body': snapshot.val().msg,
            'icon':'img/babyrage.png',
            'data': {
              'url': 'https://www.domain.com.tw'
            }
          };
          
          fetch('https://fcm.googleapis.com/fcm/send', {
            'method': 'POST',
            'headers': {
              'Authorization': 'key=AAAAmKPm1G4:APA91bEnGA-jU8MAdbqgjHFMNarwPdEC674WscDxAczqbEQntgdmfvE5_FCUtHtEWxXpGlyK3BcUtGGUaCOWczkreLnIzQqbNtouXdHhuAqqNepV8c--ZEF4P80EGx21bcfSbBwdCG2v',
              'Content-Type': 'application/json'
            },
            'body': JSON.stringify({
              'data': notification,
              'to': token,
            })
          });
    
    });

}

function init() {
    firebase.auth().onAuthStateChanged(function (u) {
        if (u) {
            user = u;
            user_email_hyphen = processEmailToHyphen(u.email);
            console.log('user:' + user.email);
            var btnLogout = document.getElementById('logout');
            btnLogout.addEventListener('click', function () {
                firebase.auth().signOut();
            });
            document.getElementById('current-user').innerText = user.email;
            database.ref(user_email_hyphen + '/chat_room').on('child_added', function (snapshot) {
                for (var i in snapshot.val()) {
                    var data = snapshot.val()[i];
                    chat_room_list.push(data);
                    var room = document.createElement('span');
                    room.className = 'dropdown-item';
                    room.innerHTML = data;
                    document.getElementById('dropdown-menu').appendChild(room);
                    room.addEventListener('click', changeRoom);

                    //add notification
                    data = processEmailToHyphen(data);
                    database.ref(user_email_hyphen+'/'+data).once('value',function(snapshot){
                        var first_cnt = 0, second_cnt = 0;
                        for(var i in snapshot.val()){
                            first_cnt++;
                        }

                        database.ref(user_email_hyphen+'/'+data).on('child_added',function(msg_ctx){
                            second_cnt++;
                            if(second_cnt > first_cnt){
                                Noti(msg_ctx);
                            }
                        });
                    });
                }
            });


        } else {
            alert('Please sign in first!');
            window.location.href = 'index.html';
        }
    });

    GEBI('user-email').addEventListener('keydown', sendUserEmail);
    GEBI('msg').addEventListener('keydown',function(evt){
        if(evt.keyCode == 13){
            clickSubmit();
        }
    });
    var submit = document.getElementById('submit');
    submit.addEventListener('click', clickSubmit);

    Notification.requestPermission();
    
}

window.onload = function () {
    init();
};

