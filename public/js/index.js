var database = firebase.database();
var provider = new firebase.auth.GoogleAuthProvider();

var login = GEBI('login');
var signInByGoogle = GEBI('sign-in-google')
var pwd = GEBI('pwd');
var email = GEBI('email');

async function clickLogin() {
    event.preventDefault();
    try {
        await firebase.auth().signInWithEmailAndPassword(email.value, pwd.value);
        var user = firebase.auth().currentUser;
        if (user) {
            console.log(user);
            window.location.href = 'chat.html';
        }
    } catch (error) {
        console.log('some error occors');
        alert(error.message);
    }
}

async function clickRegister() {
    event.preventDefault();
    try {
        await firebase.auth().createUserWithEmailAndPassword(email.value, pwd.value);
        firebase.auth().signInWithEmailAndPassword(email.value, pwd.value);
        var user = firebase.auth().currentUser;
        if (user) {
            database.ref(processEmailToHyphen(email.value)).set({
                email:email.value
            });
            window.location.href = 'chat.html';
        }
    } catch (error) {
        alert(error.message);
    }

}

GEBI('register').addEventListener('click',clickRegister);
login.addEventListener('click', clickLogin);
signInByGoogle.addEventListener('click', function () {
    firebase.auth().signInWithPopup(provider).then(function (result) {
        window.location.href = 'chat.html';
    }).catch(function (error) {
        alert(error.message);
    });
});