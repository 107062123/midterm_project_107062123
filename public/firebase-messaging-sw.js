importScripts('https://www.gstatic.com/firebasejs/7.14.1/firebase-app.js');
importScripts('https://www.gstatic.com/firebasejs/7.14.2/firebase-messaging.js');

var firebaseConfig = {
    apiKey: "AIzaSyBwZdCOmkEMdaI5Ytx14AwnSzBvFrEXQ70",
    authDomain: "midtermprojectofsoftwarestudio.firebaseapp.com",
    databaseURL: "https://midtermprojectofsoftwarestudio.firebaseio.com",
    projectId: "midtermprojectofsoftwarestudio",
    storageBucket: "midtermprojectofsoftwarestudio.appspot.com",
    messagingSenderId: "655584842862",
    appId: "1:655584842862:web:7f47e6c125213b9697827f",
    measurementId: "G-NHMYCYLP62"
};
firebase.initializeApp(firebaseConfig);
var messaging = firebase.messaging();

messaging.setBackgroundMessageHandler(function(payload) {
    var data = payload.data;
    var title = data.title;
    var options = {
      body: data.body
    };
    click_action = data.click_action;
  
    return self.registration.showNotification(title, data);
  });