# Software Studio 2020 Spring Midterm Project

## Topic
* Project Name : midtermProjectOfSoftwareStudio
* Key functions (add/delete)
    1. sendUserEmail():處理使用者邀請其他使用者聊天。
    2. changeRoom():處理使用者換聊天室。
    3. Noti(snapshot):決定瀏覽器通知的內容。

## Basic Components
|Component|Score|Y/N|
|:-:|:-:|:-:|
|Membership Mechanism|15%|Y|
|Firebase Page|5%|Y|
|Database|15%|Y|
|RWD|15%|Y|
|Topic Key Function|20%|Y|

## Advanced Components
|Component|Score|Y/N|
|:-:|:-:|:-:|
|Third-Party Sign In|2.5%|Y|
|Chrome Notification|5%|Y|
|Use CSS Animation|2.5%|Y|
|Security Report|5%|Y|

## Website Detail Description

## 作品網址：https://midtermprojectofsoftwarestudio.web.app/

# Components Description : 
1. 
![](https://i.imgur.com/arRuz8j.jpg)
輸入想邀請的對象的email來創建一個私聊聊天室。
2. 
![](https://i.imgur.com/9EZP2J7.jpg)
當創建聊天室後，便可從中挑選要進入哪個聊天室聊天。
3. 
![](https://i.imgur.com/QdKPlxB.jpg)
在聊天室輸入"babyrage"會轉換成嬰靈，這些嬰靈還活跳跳的。
4. 
![](https://i.imgur.com/WOXpJNK.png)


若使用者當前頁面不在聊天室，有其他使用者傳訊息會跳出瀏覽器通知。


